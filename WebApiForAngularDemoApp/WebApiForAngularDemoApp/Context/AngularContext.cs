﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiForAngularDemoApp.Context.Config;
using WebApiForAngularDemoApp.Context.Domain;

namespace WebApiForAngularDemoApp.Context
{
    public class AngularContext : DbContext
    {
        public AngularContext(DbContextOptions<AngularContext> options) : base(options)
        {

        }

        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserConfig());
        }
    }
}
