﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiForAngularDemoApp
{
    public static class Utility
    {
        public static string GenerateToken(string userName)
        {
            var token = userName + ":" + DateTime.UtcNow.ToString("yyyy-MM-dd_HH_mm_ss");
            return token;
        }
    }
}
