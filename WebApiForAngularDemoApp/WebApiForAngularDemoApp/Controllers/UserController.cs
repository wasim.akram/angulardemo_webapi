﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApiForAngularDemoApp.Context;
using WebApiForAngularDemoApp.Context.Domain;
using WebApiForAngularDemoApp.Repository;
using WebApiForAngularDemoApp.Repository.IRepository;

namespace WebApiForAngularDemoApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private AngularContext _context;
        private IUserRepository userRepository;

        public UserController(AngularContext context)
        {
            _context = context;
            userRepository = new UserRepository(_context);
        }

        // GET: api/Users
        [HttpGet]
        public async Task<IActionResult> GetUsers()
        {

            try
            {
                var list = await userRepository.GetAll();
                if (list != null && list.Count > 0)
                    return StatusCode(StatusCodes.Status200OK, new { data = list });
                else
                    return StatusCode(StatusCodes.Status404NotFound);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { data = ex.Message });
            }
        }

        // GET: api/Users/GetUser
        [HttpGet("GetUser")]
        public async Task<IActionResult> GetUser([FromQuery]int id)
        {
            try
            {
                object customer = await userRepository.Get(id);
                if (customer != null)
                    return StatusCode(StatusCodes.Status302Found, new { data = customer });
                else
                    return StatusCode(StatusCodes.Status404NotFound);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { data = ex.Message });
            }
        }

        // PUT: api/Users/PutUser
        [HttpPut("PutUser")]
        public async Task<IActionResult> PutUser([FromBody] User user)
        {
            try
            {
                bool result = await userRepository.Update(user);
                if (result)
                    return StatusCode(StatusCodes.Status200OK);
                else
                    return StatusCode(StatusCodes.Status404NotFound);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { data = ex.Message });
            }
        }

        // POST: api/Users/PostUser
        [HttpPost("PostUser")]
        public async Task<IActionResult> PostUser([FromBody] User user)
        {
            try
            {
                bool result = await userRepository.Add(user);
                if (result)
                    return StatusCode(StatusCodes.Status201Created, new { result = true });
                else
                    return StatusCode(StatusCodes.Status400BadRequest);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { data = ex.Message });
            }
        }

        // DELETE: api/Users/DeleteUser
        [HttpDelete("DeleteUser")]
        public async Task<IActionResult> DeleteUser([FromQuery] int id)
        {
            try
            {
                bool result = await userRepository.Delete(user=>user.Id == id);
                if (result)
                    return StatusCode(StatusCodes.Status200OK);
                else
                    return StatusCode(StatusCodes.Status404NotFound);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { data = ex.Message });
            }
        }

        // HttpGet: api/Users/Login
        [HttpGet("Login")]
        public async Task<IActionResult> Login(string userName,string password)
        {
            try
            {
                var user = await userRepository.ValidateUser(userName,password);
                if (user!=null)
                {
                    var token = Utility.GenerateToken(userName);
                    user.Token = token;
                    await userRepository.Update(user);
                    return StatusCode(StatusCodes.Status200OK,new { token=token});
                }
                else
                    return StatusCode(StatusCodes.Status401Unauthorized);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { data = ex.Message });
            }
        }

        // HttpGet: api/Users/Logout
        [HttpGet("Logout")]
        public async Task<IActionResult> Logout(string token)
        {
            try
            {
                var user = await userRepository.FindAsync(user=>user.Token==token);
                if (user != null)
                {                    
                    user.Token = "";
                    await userRepository.Update(user);
                    return StatusCode(StatusCodes.Status200OK,new { result=true});
                }
                else
                    return StatusCode(StatusCodes.Status404NotFound);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { data = ex.Message });
            }
        }

        // HttpGet: api/Users/ValidateToken
        [HttpGet("ValidateToken")]
        public async Task<IActionResult> ValidateToken(string token)
        {
            try
            {
                var user = await userRepository.FindAsync(user => user.Token == token);
                if (user != null)
                {                   
                    return StatusCode(StatusCodes.Status200OK, new { result = true });
                }
                else
                    return StatusCode(StatusCodes.Status401Unauthorized);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { data = ex.Message });
            }
        }
    }
}