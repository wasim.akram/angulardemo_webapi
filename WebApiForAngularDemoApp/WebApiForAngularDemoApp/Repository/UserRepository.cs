﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiForAngularDemoApp.Context;
using WebApiForAngularDemoApp.Context.Domain;
using WebApiForAngularDemoApp.Repository.IRepository;

namespace WebApiForAngularDemoApp.Repository
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        public UserRepository(AngularContext context) : base(context)
        {

        }

        public async Task<User> ValidateUser(string userName, string Password)
        {
            var user = await FindAsync(user => user.UserName == userName && user.Password == Password);
            return user;
        }
    }
}
