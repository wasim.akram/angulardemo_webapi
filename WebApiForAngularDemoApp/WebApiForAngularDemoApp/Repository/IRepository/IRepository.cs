﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace WebApiForAngularDemoApp.Repository.IRepository
{
    public interface IRepository<TEntity> where TEntity : class
    {
        Task<TEntity> Get(int id);
        Task<ICollection<TEntity>> GetAll();
        Task<ICollection<TEntity>> GetAll(string query);
        Task<bool> Add(TEntity entity);
        Task<bool> Update(TEntity entity);
        Task<ICollection<TEntity>> FindAsyncAll(Expression<Func<TEntity, bool>> predicate);
        Task<TEntity> FindAsync(Expression<Func<TEntity, bool>> predicate);
        Task<bool> Delete(Expression<Func<TEntity, bool>> predicate);

    }
}
