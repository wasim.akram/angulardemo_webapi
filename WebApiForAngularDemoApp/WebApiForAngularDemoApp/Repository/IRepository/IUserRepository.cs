﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiForAngularDemoApp.Context.Domain;

namespace WebApiForAngularDemoApp.Repository.IRepository
{
    public interface IUserRepository : IRepository<User>
    {
        Task<User> ValidateUser(string userName, string Password);
    }
}
