﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WebApiForAngularDemoApp.Repository.IRepository;

namespace WebApiForAngularDemoApp.Repository
{
    public class GenericRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly DbContext _context;
        protected readonly DbSet<TEntity> _dbset;

        public GenericRepository(DbContext context)
        {
            _context = context;
            _dbset = _context.Set<TEntity>();
        }

        public async Task<TEntity> Get(int id)
        {
            return await _dbset.FindAsync(id);
        }


        public async Task<ICollection<TEntity>> GetAll()
        {
            return await _dbset.ToListAsync();
        }

        public async Task<ICollection<TEntity>> GetAll(string query)
        {
            return await _dbset.FromSqlRaw(query).ToListAsync();
        }

        public async Task<ICollection<TEntity>> FindAsyncAll(Expression<Func<TEntity, bool>> predicate)
        {
            try
            {
                return await _dbset.Where(predicate).ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> Add(TEntity entity)
        {
            try
            {
                int rowAffected = 0;
                await _dbset.AddAsync(entity);
                rowAffected = await _context.SaveChangesAsync();
                return (rowAffected > 0) ? true : false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> Update(TEntity entity)
        {
            try
            {
                int rowAffected = 0;
                _dbset.Update(entity);
                rowAffected = await _context.SaveChangesAsync();
                return (rowAffected > 0) ? true : false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> Delete(Expression<Func<TEntity, bool>> predicate)
        {
            try
            {
                int rowAffected = 0;
                var objToDelete = await _dbset.SingleOrDefaultAsync(predicate);
                _context.Remove(objToDelete);
                rowAffected = await _context.SaveChangesAsync();
                return (rowAffected > 0) ? true : false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<TEntity> FindAsync(Expression<Func<TEntity, bool>> predicate)
        {
            try
            {
                return await _dbset.Where(predicate).FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
