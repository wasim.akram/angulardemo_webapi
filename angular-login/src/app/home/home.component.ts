import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../Service/user.service';
import { IToken } from '../Model/itoken';
import { IBooleanReturn } from '../Model/iboolean-return';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  token:IToken;
  booleanReturn:IBooleanReturn;
  errorMessage:string;

  constructor(
    private router: Router,
    private userService:UserService
  ) { }

  ngOnInit(): void {
  }

  logout():void{
    this.token = JSON.parse(localStorage.getItem('token'));
     localStorage.removeItem('token');
     this.userService.logout(this.token.token).subscribe({
      next: result => {
        this.booleanReturn = result;
        if(this.booleanReturn.result)
           this.router.navigate(['/login']);
      },
      error: err =>{
        this.errorMessage = err
        console.log(this.errorMessage);
      } 
    });
  }

}
