import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IUser } from "../Model/IUser";
import { UserService } from '../Service/user.service';
import { IBooleanReturn } from '../Model/iboolean-return';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  loading = false;
  submitted = false;
  user:IUser;
  booleanReturn:IBooleanReturn;
  errorMessage:string;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private userService:UserService
  ) { }

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email:['',Validators.required],
      username: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', Validators.required]
  },{
    validator: MustMatch('password', 'confirmPassword')
   });
  }

  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
        return;
    }
    this.loading = true;

    this.user={
      firstName: this.f.firstName.value,
      lastName:this.f.lastName.value,
      email:this.f.email.value,
      password:this.f.password.value,
      userName:this.f.username.value
    };
    

    this.userService.register(this.user).subscribe({
      next: result => {
        this.booleanReturn = result;
        if(this.booleanReturn.result)
           this.router.navigate(['/login']);
      },
      error: err =>{
        this.errorMessage = err
        console.log(this.errorMessage);
      } 
    });
  }  

}

// custom validator to check that two fields match
export function MustMatch(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
          // return if another validator has already found an error on the matchingControl
          return;
      }

      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
          matchingControl.setErrors({ mustMatch: true });
      } else {
          matchingControl.setErrors(null);
      }
  }
}
