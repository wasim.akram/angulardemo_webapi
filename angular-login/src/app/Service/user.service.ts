import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';
import { IToken } from '../Model/itoken';
import { IUser } from "../Model/IUser";
import { IBooleanReturn } from '../Model/iboolean-return';

@Injectable({
  providedIn: 'root'
})

export class UserService {
  private baseUrl = "https://localhost:44325/api/User/";

  constructor(private http:HttpClient) {

  }

  login(username:string,password:string):Observable<IToken>{
    var url = this.baseUrl+'Login?username='+username+'&password='+password;
    return this.http.get<IToken>(url)
      .pipe(
        tap(data => console.log('All: ' + JSON.stringify(data))),
        catchError(this.handleError)
      );

  }

  logout(token:string):Observable<IBooleanReturn>{
    var url = this.baseUrl+'Logout?token='+token;
    return this.http.get<IBooleanReturn>(url)
      .pipe(
        tap(data => console.log('All: ' + JSON.stringify(data))),
        catchError(this.handleError)
      );
  }

  validateToken(token:string):Observable<IBooleanReturn>{
    var url = this.baseUrl+'ValidateToken?token='+token;
    return this.http.get<IBooleanReturn>(url)
      .pipe(
        tap(data => console.log('All: ' + JSON.stringify(data))),
        catchError(this.handleError)
      );
  }

  register(user:IUser):Observable<IBooleanReturn>{
    var url = this.baseUrl+'PostUser'
    let body = JSON.stringify(user);
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    return this.http.post<IBooleanReturn>(url,body,httpOptions)
    .pipe(
      tap(data => console.log('All: ' + JSON.stringify(data))),
      catchError(this.handleError)
    );
  }

  private handleError(err: HttpErrorResponse) {
    // in a real world app, we may send the server to some remote logging infrastructure
    // instead of just logging it to the console
    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      errorMessage = `An error occurred: ${err.error.message}`;
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
    }
    console.error(errorMessage);
    return throwError(errorMessage);
  }
}
