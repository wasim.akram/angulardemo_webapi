import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../Service/user.service';
import { IToken } from '../Model/itoken';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  title = 'angular-login';
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  token:IToken;
  errorMessage:string;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private userService:UserService) { }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
  });
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  onSubmit(){
    this.submitted = true;

    // stop here if form is invalid
      if (this.loginForm.invalid) {
          return;
      }
      this.loading = true;
      this.userService.login(this.f.username.value,this.f.password.value).subscribe({
        next: token => {
          this.token = token;
          console.log(token);
          localStorage.setItem('token',JSON.stringify(this.token));
          this.router.navigate(['/home']);
        },
        error: err =>{
          this.errorMessage = err;
          this.loading = false;
          console.log(this.errorMessage);
        } 
      });
  }

}
