import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from './Service/user.service';
import { IToken } from './Model/itoken';
import { IBooleanReturn } from './Model/iboolean-return';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  isLoggedIn:boolean=false;
  token:IToken;
  booleanReturn:IBooleanReturn;
  errorMessage:string;

  constructor(
    private router: Router,
    private userService:UserService){

  }

  ngOnInit(): void {
    if(localStorage.getItem('token') !=null ){
      this.token = JSON.parse(localStorage.getItem('token'));
      console.log(this.token);
      this.isLoggedIn=true;
      this.router.navigate(['/home']);
    }
    else{
      this.isLoggedIn=false;
      this.router.navigate(['/login']);
    }
  }
 
}
