import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from '../Service/user.service';
import { IToken } from '../Model/itoken';
import { IBooleanReturn } from '../Model/iboolean-return';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  userToken:IToken;
  booleanReturn:IBooleanReturn;
  errorMessage:string;

  constructor(
    private userService:UserService,
    private router : Router){

  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if(localStorage.getItem('token') == null){
      alert('Not logged In');
      this.router.navigate(['/login']);
      return false;
    }
    this.userToken = JSON.parse(localStorage.getItem('token'));
    this.userService.validateToken(this.userToken.token).subscribe({
      next: result => {
        this.booleanReturn = result;
        if(!this.booleanReturn.result){
          alert('Not logged In');
          this.router.navigate(['/login']);
          return false;
        }
        return true;   
      },
      error: err =>{
        this.errorMessage = err
        console.log(this.errorMessage);
        this.router.navigate(['/login']);
        return false;
      } 
    });     
    return true;
  }
  
}
